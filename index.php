 <?php
/**
 * Licensed under the BSD 3-Clause License,
 * for full terms see LICENSE.
 *
 * Copyright (c) 2021-2023 Taavi Väänänen <hi@taavi.wtf>
 * Copyright (c)      2023 Wikimedia Foundation, Inc.
 * All rights reserved.
 */

error_reporting( -1 );
ini_set( 'display_errors', 1 );

/**
 * @return mysqli
 */
function getSqlConnection() {
    $connection = new mysqli(
        'meta.web.db.svc.wikimedia.cloud',
        getenv( 'TOOL_REPLICA_USER' ),
        getenv( 'TOOL_REPLICA_PASSWORD' ),
        'meta_p'
    );

    if ($connection->connect_error) {
        throw new RuntimeException("Connection failed: " . $connection->connect_error);
    }

    return $connection;
}

function getDbNames() {
    $connection = getSqlConnection();
    $statement = $connection->prepare('select dbname, url, is_closed, substring(slice, 1, 2) as section from wiki;');
    $statement->execute();

    if ($statement->error) {
        throw new RuntimeException("Failed to retrieve data: $statement->error");
    }

    $databases = [];

    $result = $statement->get_result();
    while ($row = $result->fetch_assoc()) {
        $databases[] = $row;
    }

    $result->close();
    $statement->close();
    $connection->close();

    return $databases;
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet"
          href="https://tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap.css">
    <script>
        // Search for a database by name, implemented using linear search
        function search() {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("search");
            filter = input.value.toUpperCase();
            table = document.querySelector("#db-table");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
    <style>
        #search {
            background-image: url('/searchicon.png');
            /* Add a search icon to input */
            background-position: 10px 12px;
            /* Position the search icon */
            background-repeat: no-repeat;
            /* Do not repeat the icon image */
            width: 100%;
            /* Full-width */
            font-size: 16px;
            /* Increase font-size */
            padding: 12px 20px 12px 40px;
            /* Add some padding */
            border: 1px solid #ddd;
            /* Add a grey border */
            margin-bottom: 12px;
            /* Add some space below the input */
        }
    </style>
    <title>Database names</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <div class="navbar-nav">
            <a class="nav-item navbar-brand" href="/">Database names</a>
            <a class="nav-item nav-link" href="https://replag.toolforge.org/">Replication lag information</a>
            <a class="nav-item nav-link" href="https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database">Documentation</a>
            <a class="nav-item nav-link" href="https://gitlab.wikimedia.org/toolforge-repos/db-names">Tool source code</a>
        </div>
    </div>
</nav>


<div class="container mt-3">
    <p>
        This is a list of Wikimedia wikis available on the
        <a href="https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database">wiki replicas</a>
        with a <code>_p</code> suffix. This information is queried from the
        special <code>meta_p</code> database.
    </p>
    <input type="text" id="search" onkeyup="search()" placeholder="Search by URL..">
    <div class="overflow-scroll">
        <table class="table table-hover table-sm" id="db-table">
            <thead>
                <tr>
                    <th scope="col">URL</th>
                    <th scope="col">Status</th>
                    <th scope="col">Database name</th>
                    <th scope="col">Section</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach(getDbNames() as $row): ?>
                <tr>
                    <td><a href="<?= $row['url'] ?>"><?= $row['url'] ?></a></td>
                    <td><?= $row['is_closed'] ? '<span class="badge text-bg-dark">closed</span>' : '' ?></td>
                    <td><?= $row['dbname'] ?></td>
                    <td><code><?= $row['section'] ?></code></td>
                </tr>
                <?php endforeach; ?>
           </tbody>
        </table>
    </div>
</div>
</body>
</html>

